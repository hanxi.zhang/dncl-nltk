import IndexToEs
import ExtractNe
import NeDistillation

import nltk
import sys
import csv
import re
import traceback

ES_INDEX = 'dncl2017' 
DNCL_CSV_FILE = 'dncl-4year-2012-2015.csv'

WHO_PATTERNS = ['from', 'customer of', 'client of', 'on behalf of', 'representing', 'tired of']
WHAT_PATTERNS = ['regarding', 'with regard to', 'qualify for', 'calling about', 'offering', 'discounted']

#will be populated once to either the who or what pattern, based on the commandline argument provided
pattern_dict = None

#The output of this program: a set of "possible" named entities (NEs)
#They could be either the whos (who called consumers), or the whats (what products/services), depending on whether who or what is specified in the commandline
topics = set()

#The 'distilled' copy of topics, the final objective of this program
distilled_topics = set()

if len(sys.argv) < 2 or sys.argv[1] not in ['who', 'what', 'index']:
        print "Usage: index | who | what"
	print "Step 1. run this program with 'index' option to pre-polulate complaints into local ElasticSearch\n"
	print "Step 2. run this program with 'who' or 'what' option to get a set of entities of potential interests\n"
 
	sys.exit(1)

elif sys.argv[1] == "who":
        print "Trying to find who called...\n"
	pattern_dict = WHO_PATTERNS
elif sys.argv[1] == "what":
        print "Trying to find what product/service is being offered...\n"
	pattern_dict = WHAT_PATTERNS
elif sys.argv[1] == 'index':
	#First index complaints to ES, current we care about date, comments and tele-marketing phone numbers, can add more columns later
	IndexToEs.dncl_complaints_to_es(ES_INDEX, DNCL_CSV_FILE)
	sys.exit(0)

with open(DNCL_CSV_FILE) as fhandle:
	reader = csv.DictReader(fhandle, delimiter=',')

	for line in reader:
		#skip those complaints with no comments
		if None == line["Complainant comments"] or '' == line["Complainant comments"].strip(): 
			continue

		comment = line["Complainant comments"].strip()

		#For each who or what pattern
		for pattern in pattern_dict:

			#If the pattern occurs at least once, try to find all occurances of the SAME pattern
			#E.g., ... ... from ... ... from ... ...
			if pattern in comment:
				try:
					#Tokenize the comment into a bag of words first. Note that using nltk's word_tokenize method is slightly different from python's string split() method
					#For text = "Yesterday I received a call from who is calling from India."
					#nltk.word_tokenize(text) will yield ['Yesterday', 'I', 'received', 'a', 'call', 'from', 'who', 'is', 'calling', 'from', 'India', '.']
					#text.split() will yield ['Yesterday', 'I', 'received', 'a', 'call', 'from', 'who', 'is', 'calling', 'from', 'India.']

					tokens = nltk.word_tokenize(comment)

					#if the first word of pattern occurs more than once, obtain all the start positions
					#for example ... on behalf of ... on ... 
					
					#There should be bettern approaches, e.g., based on regex. The approach I have here "works".
					indexes = [i for i,e in enumerate(tokens) if e == pattern.split(' ')[0]]

					#For each 'possible' beginning position
					for index in indexes:
						#Get next x number of words, where x is the pattern's word count
						#I.e., x in pattern 'on behalf of' is 3	
						beginning = tokens[index:][:len(pattern.split(' '))]

						#if this is indeed a match of the pattern, instead of just a match on the first word
						if " ".join(beginning) == pattern:

							#Get the rest of the tokens from the index + word-count-of-the-pattern position
							chunk_after_beginning_of_pattern = tokens[index:][len(pattern.split(' ')):]

							#print pattern + ' ' +  str(chunk_after_beginning_of_pattern)

							#nltk tagging is a step where, the grammar of each word is marked in the context of the sentence
							#For example
							#nltk.pos_tag(['Yesterday', 'I', 'received', 'a', 'call', 'from', 'an', 'agent', 'calling', 'from', 'India', '.']) will yield
							#[('Yesterday', 'NN'), ('I', 'PRP'), ('received', 'VBD'), ('a', 'DT'), ('call', 'NN'), ('from', 'IN'), ('an', 'DT'), ('agent', 'NN'), 
							# ('calling', 'VBG'), ('from', 'IN'), ('India', 'NNP'), ('.', '.')]

							tagged = nltk.pos_tag(chunk_after_beginning_of_pattern)

							#print str(tagged)

							#This is a step where, we trim unwanted words(noises) based on words' grammatical markups
							#Essentially we only keep noun and noun-like words plus some conjuctions that are likely to be of interest
							#More in-line comment in the getTopic() method
							topic = ExtractNe.getTopic(tagged)

							#print topic

							#If the returned topic is not empty, add it to the set
							if len(topic) > 0:
								topics.add(topic) 


				except Exception as e:
					#print(sys.exc_info()[0])
					#Does not handle unicodes well, expect some <type 'exceptions.UnicodeDecodeError'> exceptions for certain comments
					#For now ignore exceptions since this is a best effort approach
					pass

distilled_topics = NeDistillation.distill(topics, ES_INDEX)

for distilled in distilled_topics:
	count = NeDistillation.countTopic(distilled, ES_INDEX)
	print distilled + ' is found ' + str(count) + ' times.'

print '\n' + str( len(topics) ) + ' raw topics found.'

print str( len(distilled_topics) ) + ' distilled topics found.'

