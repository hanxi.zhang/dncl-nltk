import elasticsearch
import csv
import codecs
import sys

#The complaints doc_type should have the following schema:
#	a date field of type "date"
#	a comment field of type "string"
#	and a telenum field of type "string"
MY_DOC_TYPE = 'complaints'

def dncl_complaints_to_es(index_name, dncl_csv_file):


	#Tested with ElasticSearch 2.4, and should be easily adaptable to newer versions of ES such as 5.3
	es = elasticsearch.Elasticsearch()  # use default of localhost, port 9200

	#drop all entries in the index and re-populate
	es.indices.delete(index=index_name, ignore=[400, 404])
	print 'Index ' + str(index_name) + ' has been reset!\n' 

	#So that we won't choke on unicode-based comments
	reload(sys)
	sys.setdefaultencoding('utf8')

	print 'Indexing DNCL data into Elastic Search...\n'

	with codecs.open(dncl_csv_file, 'r', 'utf-8-sig') as fhandle:

		reader = csv.DictReader(fhandle, delimiter=',')

		cursor = 0

		for line in reader:

			date = None

			tmp = line["DateFiled"].strip().split()
			if len(tmp) == 0:
				continue

			#Get a non-empty date value
			date = tmp[0]

			comment = line["Complainant comments"]
			if comment == None or comment == '':
				continue

			#Get a non-empty comment value
			comment = comment.strip()

			telenum = line["Telemarketing phone number"]

			#Only index those records with non-empty date and comment values
			es.index(index=index_name, doc_type=MY_DOC_TYPE, id=cursor, body={
				'date': date,
				'telenum': telenum,
				'comment': comment
			})

			cursor = cursor + 1

	print 'Done indexing DNCL complaints data into ElasticSearch'
