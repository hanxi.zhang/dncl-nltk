from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from pprint import pprint
import csv

#Minimum times a raw topic should be found in the whole dataset, in order for us to keep it as a meaningful record
MIN_OCCURENCE = 3

#Some common start characters to be ignored in raw topic string
TO_ESCAPE = ["'", ".", "*", "+", "-", "%", "@", "$", "#"]

def distill(topics, index_name):

	result = set()
	client = Elasticsearch()

	for topic in topics:
		first_char = topic[0]
		if first_char in TO_ESCAPE:
			topic = topic.replace(first_char, '', 1)

		if topic.isdigit() or topic in result:
			continue

		#Can apply some python es-dsl techniques here: 
		#Capping with 'extra', filtering, sorting the results based on date, etc
		#s = Search(using=client, index=index_name, extra={"size": 5000}).query("query_string", query="\"" + topic + "\"").filter('range', date={'gte':'2012-01-01', 'lte':'2015-01-01'})

		s = Search(using=client, index=index_name).query("query_string", query="\"" + topic + "\"")
		response = s.execute()

		#We will only keep it if it is recurring enough, thus ruling out lots of gibberish
		if(len(response) >= MIN_OCCURENCE):
			result.add(topic)

	#Sort results in alphabetical order, so clusters are visually more obvious, e.g.,
	#MICROSOFT
	#MICROSOFT CENTRE
	#MICROSOFT SUPPORT
	#MICROSOFT TECH
		
	return sorted(result)

def countTopic(topic, index_name):

	client = Elasticsearch()

	#s = Search(using=client, index=index_name).query("query_string", query="\"" + topic + "\"").filter('range', date={'gte':'2012-01-01', 'lte':'2015-01-01'})
	s = Search(using=client, index=index_name).query("query_string", query="\"" + topic + "\"")
	response = s.execute()

	return len(response)

#E.g., trying to find fuzzy matches on a topic like 'MARIOTE HOTTE'
def fuzzySearch(fuzzy_topic, index_name):

	client = Elasticsearch()

	fuzzy_q =  { "match": {"comment": {"query": fuzzy_topic, "fuzziness": "AUTO"}} }

	s = Search(using=client, index=index_name).query(fuzzy_q).sort({"date":{"order":"desc"}})
	response = s.execute()

	return len(response)
