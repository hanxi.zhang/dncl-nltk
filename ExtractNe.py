import nltk
import sys
import csv
import re
import traceback

NOUN_PATTERNS = ['NN', 'NNS', 'NNP', 'NNPS']
CONJUNCTION_PATTERNS = ['JJ', 'IN', 'CC']
ESCAPE_WORDS_FILE = 'escape-words-dict.txt'

#Given a list of NLTK-tagged tokens, return possible named entity using a simple heuristic
def getTopic(tags):
	result = []
	found_noun_yet = False

	escape_words = []
	with open(ESCAPE_WORDS_FILE) as f:
	        for line in f:
                	escape_words.append(line.strip().upper())

	for tag in tags:
		word, grammar = tag[0], tag[1]

		#As soon as noun pattern discontinues, break
		if found_noun_yet and grammar not in NOUN_PATTERNS:
			break

		#If a noun pattern is found, set this flag to True
		if grammar in NOUN_PATTERNS:
			found_noun_yet = True

		#If the word's grammer is in noun or other patterns 
		if grammar in NOUN_PATTERNS or grammar in CONJUNCTION_PATTERNS:
			#If the word is neither one of the common/generic words nor pure digits, append it to the result
			if word.upper() not in escape_words and re.match("^[0-9-]*$", word) is None: 
				result.append(word.upper())

	return ' '.join(result).strip()

#Possible alternative to the getTopic method, not fully tested and commented out for now
#Given a full un-tokenized sentence, return possible named entity using NLTK's tree traversal approach
#def topic2ner(sentence):
#
#	ner = []
#
#	tokens = nltk.word_tokenize(sentence)
#	pos_tags = nltk.pos_tag(tokens)
#
#	namedEnt = nltk.ne_chunk(pos_tags, binary=True)
#
#	for node in namedEnt.subtrees():
#		if node.label() == "NE":
#			for leaf in node.leaves():
#				ner.append( str(leaf) )
#
#	return ' '.join(ner).upper()


